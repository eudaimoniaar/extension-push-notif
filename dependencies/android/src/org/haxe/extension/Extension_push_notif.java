package org.haxe.extension;


import android.app.Activity;
import android.content.res.AssetManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;

import android.util.Log;
import android.lib.gcm.GcmUtils;
import android.lib.gcm.GcmRegistrar;
import android.lib.gcm.GcmIntentService;
import org.haxe.lime.HaxeObject;

import android.provider.Settings.Secure;

import java.util.LinkedList;

/* 
	You can use the Android Extension class in order to hook
	into the Android activity lifecycle. This is not required
	for standard Java code, this is designed for when you need
	deeper integration.
	
	You can access additional references from the Extension class,
	depending on your needs:
	
	- Extension.assetManager (android.content.res.AssetManager)
	- Extension.callbackHandler (android.os.Handler)
	- Extension.mainActivity (android.app.Activity)
	- Extension.mainContext (android.content.Context)
	- Extension.mainView (android.view.View)
	
	You can also make references to static or instance methods
	and properties on Java classes. These classes can be included 
	as single files using <java path="to/File.java" /> within your
	project, or use the full Android Library Project format (such
	as this example) in order to include your own AndroidManifest
	data, additional dependencies, etc.
	
	These are also optional, though this example shows a static
	function for performing a single task, like returning a value
	back to Haxe from Java.
*/

class GcmReg extends GcmRegistrar {

	public GcmReg()
    {
        super(Extension_push_notif.getContext(), Extension_push_notif.getMainActivity()); // chain to Superclass(int) constructor
    }

	@Override
	protected void onRegister(String registrationId)
	{
		Log.i("trace", "registrationId: " + registrationId);
		Extension_push_notif.registrationId = registrationId;
		Extension_push_notif.sendRegistrationId();
	}

	@Override
	protected void onError(Exception exception)
	{
		Log.i("trace", "registrationId error");
	}
}

public class Extension_push_notif extends Extension {

	private static LinkedList<String> pushQueue; 
	
	public static HaxeObject sessionStatusListener;

	public static String registrationId;

	public static String pushData;

	public static void init(HaxeObject listener, String senderId)
	{
		sessionStatusListener = listener;

    	GcmReg gcmRegistrar = new GcmReg();

    	gcmRegistrar.registerInBackground(senderId);

    	if (pushQueue != null) {
    		processPushQueue();
    	} 
    }

    public static String getDeviceId() {
		return Secure.getString(getContext().getContentResolver(), Secure.ANDROID_ID);
    }

    public static Context getContext()
    {
    	return mainContext;
    }

    public static Activity getMainActivity()
    {
    	return mainActivity;
    }

    public static void openedFromPush(){
		if (sessionStatusListener == null) {
			
			if(pushQueue == null){
				pushQueue = new LinkedList();
			}
			pushQueue.add(pushData);
		} else {
			sendOpenedFromPushData(pushData);
		}

		pushData = null;     	  	
    }

    public static void sendRegistrationId(){
    	mainActivity.runOnUiThread(new Runnable() {
			public void run() {
				sessionStatusListener.call("onRegistered", new Object[] {registrationId});
			}
		});
    }
   
	/**
	 * Called when the activity is starting.
	 */
	public void onCreate(Bundle savedInstanceState)
	{
		if (!GcmUtils.checkPlayServices(mainActivity))
			Log.i("trace", "no google play services");
	}
	
	/**
	 * Perform any final cleanup before an activity is destroyed.
	 */
	public void onDestroy()
	{
				
	}

	private static void processPushQueue(){
		while(pushQueue.isEmpty() != true) {
			String _pushData = pushQueue.remove();
			sendOpenedFromPushData(_pushData);
		}
	}

	private static void sendOpenedFromPushData(final String _pushData){
		mainActivity.runOnUiThread(new Runnable() {
 			public void run() {
 				sessionStatusListener.call("onOpenedFromPush", new Object[] {_pushData});
 			}
 		});
	}
	
	/**
	 * Called after {@link #onRestart}, or {@link #onPause}, for your activity 
	 * to start interacting with the user.
	 */
	public void onResume()
	{
		GcmUtils.checkPlayServices(mainActivity);

		Intent intent = this.getMainActivity().getIntent();
		if (intent != null) {
			Bundle extras = intent.getExtras();

			if (extras != null) {
				if(extras.containsKey("OPENED_FROM_PUSH") && (intent.getFlags() & Intent.FLAG_ACTIVITY_LAUNCHED_FROM_HISTORY) == 0){
					if (extras.getBoolean("OPENED_FROM_PUSH", false) && !extras.getString("PUSH_DATA").isEmpty()) {
						pushData = extras.getString("PUSH_DATA");
						this.openedFromPush();
					}
				}
			}
		}		   
	}
}