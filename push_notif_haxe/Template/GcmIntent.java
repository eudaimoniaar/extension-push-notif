package ::APP_PACKAGE::;

import android.util.Log;
import android.content.Intent;
import android.lib.gcm.GcmIntentService;

import android.app.NotificationManager;
import android.content.Context;
import android.app.PendingIntent;
import android.support.v4.app.NotificationCompat;
import android.os.Bundle;
import android.os.Build;
import java.util.Set;
import org.json.JSONObject;
import org.json.JSONException;
import org.haxe.extension.Extension_push_notif;

public class GcmIntent extends GcmIntentService {

	public static final int NOTIFICATION_ID = 1;

	public GcmIntent()
	{
		super("pushTest");
	}

	private void sendNotification(String title, String msg)
	{
        NotificationManager mNotificationManager = (NotificationManager)
                this.getSystemService(Context.NOTIFICATION_SERVICE);

        Intent intent = new Intent(this, MainActivity.class);
        intent.putExtra("OPENED_FROM_PUSH", true);

        PendingIntent contentIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_CANCEL_CURRENT);

        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(this)
        .setSmallIcon(R.drawable.ic_stat_notify)
        .setContentTitle(title)
        .setStyle(new NotificationCompat.BigTextStyle()
        .bigText(msg))
        .setContentText(msg);

        mBuilder.setContentIntent(contentIntent);
        mBuilder.setAutoCancel(true);

        mNotificationManager.notify(NOTIFICATION_ID, mBuilder.build());
    }

	@Override
	protected void onSendError()
	{
		Log.i("trace", "send error");
	}

	@Override
	protected void onMessageDeleted(int total)
	{
		Log.i("trace", "message deleted");
	}

	@Override
	protected void onMessageReceived(Intent intent)
	{
		Log.i("trace", "message received");
		
		String title = intent.getStringExtra("contentTitle");
		String msg = intent.getStringExtra("message");

		Boolean sendNotif = false;

		if (Extension_push_notif.getMainActivity() == null)
			sendNotif = true;
		else if (!Extension_push_notif.getMainActivity().hasWindowFocus())
			sendNotif = true;

		if (sendNotif){
			Bundle extras = intent.getExtras(); 
			JSONObject json = new JSONObject();
			Set<String> keys = extras.keySet();
			for (String key : keys) {
			    try {

			        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
			        	json.put(key, JSONObject.wrap(extras.get(key)));
			        } else {
			        	json.put(key, extras.get(key));
			        }
			    
			    } catch(JSONException e) {
			    	
			    	Log.i("trace", e.toString());
			    
			    }
			}
			
			Extension_push_notif.pushData = json.toString();
			
			sendNotification(title, msg);
		}
	}
}