#ifndef _PUSH_NOTIFICATIONS_H_
#define _PUSH_NOTIFICATIONS_H_

namespace extension_push_notifications {

	void init();

	void onRegisterForRemoteNotifications(const char *token);
	void onRegisterForRemoteNotificationsFail(const char *error);

	void onReceiveRemoteNotification(const char* pushData);

	const char* getDeviceId();
}

#endif