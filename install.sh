#!/bin/bash

echo 'Register extension-push-notif with haxelib'
haxelib dev extension-push-notif `pwd`

if [ $? -eq 0 ]; then
  echo '[OK]'
else
  echo '[ERROR]'
fi

