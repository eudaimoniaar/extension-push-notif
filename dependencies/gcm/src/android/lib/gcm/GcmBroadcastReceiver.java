package android.lib.gcm;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.support.v4.content.WakefulBroadcastReceiver;
import android.text.TextUtils;
import android.util.Log;
//import ::APP_PACKAGE::.GcmIntent;

/**
 * {@link WakefulBroadcastReceiver} that receives GCM messages and delivers them to
 * an application-specific {@link GcmBaseIntentService} subclass.
 */
public class GcmBroadcastReceiver extends WakefulBroadcastReceiver {

    @Override
    public final void onReceive(final Context context, final Intent intent) {
        
        final String className = "::APP_PACKAGE::" + ".GcmIntent";

        Log.i("trace", "push received");
       
        ComponentName comp = new ComponentName(context.getPackageName(), className);
        // Start the service, keeping the device awake while it is launching.
        startWakefulService(context, (intent.setComponent(comp)));
        setResultCode(Activity.RESULT_OK);
    }
}
