package ar.com.euda.openfl;

#if cpp
import cpp.Lib;
#elseif neko
import neko.Lib;
#end

#if (android && openfl)
import openfl.utils.JNI;
#end

class Extension_push_notif {

	public static var instance(get, null):Extension_push_notif;
	private static var _instance:Extension_push_notif;

	private var senderId:String = "";
	public var regId:String = "";
	public var pushData:Dynamic = null;
	private var registrationCallback:Dynamic;
	private var openedFromPushCallback:Dynamic;

	public static function get_instance():Extension_push_notif
	{
		if(_instance!=null) return _instance;

		_instance = new Extension_push_notif();
		return _instance;
	}

	private function new()
	{
		
	}

	public function getDeviceId():String
	{
		#if(openfl)
		return extension_push_notif_get_device_id_jni();
		#end
		
		return "";
	}

	public function init(senderId:String, registrationCallback:Dynamic, openedFromPushCallback:Dynamic):Void
	{
		trace("init de pushes (haxe)");

		this.senderId = senderId;
		this.registrationCallback = registrationCallback;
		this.openedFromPushCallback = openedFromPushCallback;

		#if (openfl)
		#if (android)
		extension_push_notif_init_jni(get_instance(), senderId);
		#elseif (ios)
		extension_push_notif_init_jni(get_instance().onRegistered, get_instance().onOpenedFromPush);
		#end
		#end
	}
	
	#if (openfl && ios)
	private static var extension_push_notif_init_jni = Lib.load ("extension_push_notification", "extension_push_notifications_init", 2);
	private static var extension_push_notif_get_device_id_jni = Lib.load ("extension_push_notification", "extension_push_notifications_getDeviceId", 0);
	#elseif (openfl && android)
	private static var extension_push_notif_init_jni = JNI.createStaticMethod ("org.haxe.extension.Extension_push_notif", "init", "(Lorg/haxe/lime/HaxeObject;Ljava/lang/String;)V");
	private static var extension_push_notif_get_device_id_jni = JNI.createStaticMethod ("org.haxe.extension.Extension_push_notif", "getDeviceId", "()Ljava/lang/String;");
	#end

	public function onRegistered(regId:String)
	{
		trace("on registered (Haxe)");
		this.regId = regId;
		registrationCallback();
	}

	public function onOpenedFromPush(pushData:String){
		trace("on opened from push! (Haxe)");
		this.pushData = haxe.Json.parse(pushData);
		openedFromPushCallback();
	}
}
