#import <PushNotifications.h>

#import <PushNotificationsAppDelegate.h>
#import <OpenFlAppDelegate.h>
#import <UIKit/UIKit.h>


namespace extension_push_notifications {

	PushNotificationsAppDelegate *callbacks;

	void init() {
		callbacks = [[PushNotificationsAppDelegate alloc] init];

		[[OpenFlAppDelegate sharedInstance] addAppDelegate:callbacks];

		if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)
        {
            [[UIApplication sharedApplication] registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge) categories:nil]];
            [[UIApplication sharedApplication] registerForRemoteNotifications];
        }
        else
        {
            [[UIApplication sharedApplication] registerForRemoteNotificationTypes: (UIUserNotificationTypeBadge | UIUserNotificationTypeSound | UIUserNotificationTypeAlert)];
        }
	}

	const char* getDeviceId(){
        NSString *UUID = [[NSUserDefaults standardUserDefaults] objectForKey:@"userIDKey"];
        if (!UUID) {
            CFUUIDRef uuid = CFUUIDCreate(NULL);
            UUID = (NSString *)CFUUIDCreateString(NULL, uuid);
            CFRelease(uuid);

            [[NSUserDefaults standardUserDefaults] setObject:UUID forKey:@"userIDKey"];
            [[NSUserDefaults standardUserDefaults] synchronize];
        }
        const char* returnValue = [UUID UTF8String];
        return returnValue;
    }
}