package android.lib.gcm;

import java.io.IOException;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Message;
import android.preference.PreferenceManager;
import android.text.TextUtils;

import com.google.android.gms.gcm.GoogleCloudMessaging;

import android.util.Log;
import android.app.Activity;

/**
 * The base class responsible for handling registration with the GCM service.
 */
public abstract class GcmRegistrar {
    public static final String PREF_REGISTRATION_ID = "GCM_REGISTRATION_ID"; //$NON-NLS-1$

    private final Context           context;
    private final SharedPreferences preferences;
    private final Activity mainActivity;

    /**
     * Creates a new instance of {@link GcmRegistrar}.
     * @param context application's context.
     */
    public GcmRegistrar(final Context context, final Activity mainActivity)
    {
        this.context     = context;
        this.mainActivity = mainActivity;
        this.preferences = PreferenceManager.getDefaultSharedPreferences(context);
    }

    /**
     * Returns the registration ID previously returned by the GCM service.
     * @return the registration ID previously returned by the GCM service.
     */
    public final String getRegistrationId()
    {
        return this.preferences.getString(GcmRegistrar.PREF_REGISTRATION_ID, null);
    }

    /**
     * Registers the device with the GCM service.
     * @param senderId the sender ID of the GCM service.
     */
    public void registerInBackground(final String senderId)
    {
        mainActivity.runOnUiThread
        (
            new Runnable()
            {
                @Override
                public void run()
                {
                    AsyncTask<String, Void, Object> requestTask = new AsyncTask<String, Void, Object>()
                    {
                        @SuppressWarnings("synthetic-access")
                        @Override
                        protected Object doInBackground(String... params)
                        {
                            try
                            {
                                Log.i("trace", "registering2");

                                String registrationId = GoogleCloudMessaging.getInstance(GcmRegistrar.this.context).register(params[0]);
                                String storedId = GcmRegistrar.this.preferences.getString(GcmRegistrar.PREF_REGISTRATION_ID, null);

                                if (!TextUtils.isEmpty(registrationId) && !registrationId.equals(storedId))
                                {
                                    GcmRegistrar.this.preferences.edit().putString(GcmRegistrar.PREF_REGISTRATION_ID, registrationId).commit();
                                }

                                return registrationId;
                            }
                            catch (IOException e)
                            {
                                return e;
                            }
                        }

                        @Override
                        protected void onPostExecute(Object result)
                        {
                            if (result instanceof String)
                            {
                                GcmRegistrar.this.onRegister((String)result);
                            }
                            else
                            {
                                GcmRegistrar.this.onError((Exception)result);
                            }
                        }
                    };
                    requestTask.execute(senderId);
                }
            }
        );
    }

    /**
     * Called after a device has been registered.
     * @param registrationId the registration ID returned by the GCM service.
     */
    protected abstract void onRegister(String registrationId);

    /**
     * Called on registration error.
     * @param exception the exception thrown during registration.
     */
    protected abstract void onError(Exception exception);
}
