#include <PushNotificationsAppDelegate.h>
#include <PushNotifications.h>
#include <OpenFlAppDelegate.h>

@implementation PushNotificationsAppDelegate


- (void)application:(UIApplication*)application didReceiveRemoteNotification:(NSDictionary *)userInfo {
    if (application.applicationState == UIApplicationStateActive) return;

    [self onReceiveRemoteNotification:userInfo];
}

- (void)onReceiveRemoteNotification:(NSDictionary *)userInfo {
    NSLog(@"REMOTE NOTIFICATION INFO: %@", userInfo);
    NSString* jsonString = @"{}";

    NSDictionary* aps = [userInfo objectForKey: @"aps"];

    if(aps != NULL) {
        NSError *error;
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:aps
                                                    options: 0
                                                    error:&error];

        if (!jsonData) {
            NSLog(@"JSON error: %@", error.localizedDescription);
        } else {
            jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        } 

        NSLog(@"JSON to parse: %@", jsonString);
        extension_push_notifications::onReceiveRemoteNotification([jsonString UTF8String]);

    }

    [[UIApplication sharedApplication] setApplicationIconBadgeNumber: 0];
    [[UIApplication sharedApplication] cancelAllLocalNotifications];
}

- (void)application:(UIApplication*)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData*)deviceToken {
    if (deviceToken.length > 0) {
        NSLog(@"didRegister");
        NSString * token = [NSString stringWithFormat:@"%@", deviceToken];
        token = [token stringByReplacingOccurrencesOfString:@" " withString:@""];
        token = [token stringByReplacingOccurrencesOfString:@">" withString:@""];
        token = [token stringByReplacingOccurrencesOfString:@"<" withString:@""];
        NSLog(@"My token is: %@", token);
        extension_push_notifications::onRegisterForRemoteNotifications([token UTF8String]);
    }
}

- (void)application:(UIApplication*)application didFailToRegisterForRemoteNotificationsWithError:(NSError*)error {
    NSLog(@"Failed to get token, error: %@", error);
    extension_push_notifications::onRegisterForRemoteNotificationsFail([[error localizedDescription] UTF8String]);
}

- (BOOL)application:(UIApplication *)application willFinishLaunchingWithOptions :(NSDictionary *)launchOptions {
    UILocalNotification *notification = [launchOptions objectForKey:UIApplicationLaunchOptionsRemoteNotificationKey];
    if (notification) {
        [self onReceiveRemoteNotification:(NSDictionary*)notification];
    }

    return YES;
}

@end