#ifndef STATIC_LINK
#define IMPLEMENT_API
#endif

#if defined(HX_WINDOWS) || defined(HX_MACOS) || defined(HX_LINUX)
#define NEKO_COMPATIBLE
#endif


#include <hx/CFFI.h>
#include "PushNotifications.h"

#define safe_alloc_string(a) (alloc_string(a!=NULL ? a : ""))
#define safe_val_call1(func, arg1) if (func!=NULL) val_call1(func->get(), arg1)


AutoGCRoot* _onRegisterForRemoteNotifications = 0;
AutoGCRoot* _onRegisterForRemoteNotificationsFail = 0;
AutoGCRoot* _onReceiveRemoteNotification = 0;

void extension_push_notifications::onRegisterForRemoteNotifications(const char *token) {
    safe_val_call1(_onRegisterForRemoteNotifications, safe_alloc_string(token));
}

void extension_push_notifications::onRegisterForRemoteNotificationsFail(const char *error) {
    safe_val_call1(_onRegisterForRemoteNotificationsFail, safe_alloc_string(error));
}

void extension_push_notifications::onReceiveRemoteNotification(const char* pushData) {
    safe_val_call1(_onReceiveRemoteNotification, safe_alloc_string(pushData));
}


static void extension_push_notifications_init(value onRegisterForRemoteNotificationsCallback, value onReceiveRemoteNotificationCallback){
    
    _onRegisterForRemoteNotifications = new AutoGCRoot(onRegisterForRemoteNotificationsCallback);

    _onReceiveRemoteNotification = new AutoGCRoot(onReceiveRemoteNotificationCallback);

	extension_push_notifications::init();
}
DEFINE_PRIM(extension_push_notifications_init, 2);

static void extension_push_notifications_setOnRegisterForRemoteNotificationsFail(value onRegisterForRemoteNotificationsFailCallback){
    
    _onRegisterForRemoteNotificationsFail = new AutoGCRoot(onRegisterForRemoteNotificationsFailCallback);
}
DEFINE_PRIM(extension_push_notifications_setOnRegisterForRemoteNotificationsFail, 1);

static value extension_push_notifications_getDeviceId(){

	return safe_alloc_string(extension_push_notifications::getDeviceId());
}
DEFINE_PRIM(extension_push_notifications_getDeviceId, 0);

extern "C" void extension_push_notif_main () {
	val_int(0); // Fix Neko init
}
DEFINE_ENTRY_POINT (extension_push_notif_main);

extern "C" int extension_push_notif_register_prims () {
    return 0;
}

