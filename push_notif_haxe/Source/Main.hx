package;

import openfl.display.Sprite;
import ar.com.euda.openfl.Extension_push_notif;

#if cpp
import cpp.Lib;
#elseif neko
import neko.Lib;
#end

class Main extends Sprite {
	
	var senderId:String = "76194840040";
	
	public function new () {
		
		super ();
		
		trace("app started");

		Extension_push_notif.get_instance().init(senderId, function() {
			trace(Extension_push_notif.get_instance().regId);
		}, function(){
			var pushData = Extension_push_notif.get_instance().pushData;
			trace("Opened from push: " + pushData);
		});

		var deviceID:String = Extension_push_notif.get_instance().getDeviceId();

		trace(deviceID);
	}	
}